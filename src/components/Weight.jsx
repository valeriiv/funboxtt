import React from "react";
import classNames from "classnames";
import "../assets/styles/weight.scss";

const Weight = props => ({
  render() {
    const { color, deactivated } = this.props;
    const weights = classNames({
      weight__wrapper: color === "blue",
      "weight__wrapper-red": color === "red",
      "weight__wrapper-green": color === "green",
      "weight__wrapper-disabled": deactivated
    });
    return (
      <div className="weight">
        <div className={weights}>
          <div className="weight__wrapper-value">{props.boxWeight}</div>
          <div className="weight__wrapper-unit">{props.boxWeightUnit}</div>
        </div>
      </div>
    );
  }
});

export default Weight;
