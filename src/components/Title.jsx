import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import "../assets/styles/title.scss";

const Title = (props) => ({
  render() {
      const differentTitle = classNames({'title__wrapper-value' : props.size36,
      'title__wrapper-value-del-g' : props.size16,
      'title__wrapper-value-name-big': props.size48,
      'title__wrapper-value-name-med': props.size24,
      'title__wrapper-value-gift': props.size14pc || props.size14gift || props.size14customer || props.size13,
      'title__wrapper-value-name-big-disable': props.size48 && props.deactivated,
      'title__wrapper-value-name-med-disable': props.size24 && props.deactivated
    });
    return (
      <div className="title__wrapper">
      {props.size36 ?
        <span className={differentTitle}>{props.headerTitle}</span>
        :
        props.size48 ?
        <span className={differentTitle}>{props.boxName}</span>
        :
        props.size48 && props.deactivated ?
        <span className={differentTitle}>{props.boxName}</span>
        :
        props.size24 ?
        <span className={differentTitle}>{props.boxDescr}</span>
        :
        props.size24 && props.deactivated ?
        <span className={differentTitle}>{props.boxDescr}</span>
        :
        props.size14pc ?
        <span className={differentTitle}>{props.portionCount}</span>
        :
        props.size14gift ?
        <span className={differentTitle}>{props.portionGift}</span>
        :
        props.size14customer ?
        <span className={differentTitle}>{props.customer}</span>
        :
        props.size13 ?
        <span className={differentTitle}>{props.bottomTitle}</span>
        :
        <span className={differentTitle}>{props.delicacyTitle}</span>
      }
      </div>
    );
  }
});

Title.propTypes = {
  headerTitle: PropTypes.string
};

Title.defaultProps = {
  headerTitle: "Ты сегодня покормил кота?"
};

export default Title;
