import React, { Component } from "react";
import "./assets/styles/App.scss";
import Title from "./components/Title.jsx";
import ContentList from "./containers/ContentList.jsx";

class App extends Component {
  constructor() {
    super();
    this.state = {
      headerTitle: "Ты сегодня покормил кота?",
      contents: [
        {
          delicacyTitle: "Сказочное заморское яство",
          boxName: "Нямушка",
          boxDescr: "с фуа-гра",
          portionCount: "10 порций",
          portionGift: "мышь в подарок",
          boxWeight: "0,5",
          boxWeightUnit: "кг",
          color: "blue",
          customer: "заказчик доволен",
          bottomTitle: "Чего сидишь? Порадуй котэ,",
          bottomTitleEnd: "купи.",
          deactivated: false
        },
        {
          delicacyTitle: "Сказочное заморское яство",
          boxName: "Нямушка",
          boxDescr: "с рыбой",
          portionCount: "40 порций",
          portionGift: "2 мыши в подарок",
          boxWeight: "2",
          boxWeightUnit: "кг",
          color: "red",
          customer: "заказчик доволен",
          bottomTitle: "Головы щучьи с чесноком да свежайшая сёмгушка.",
          bottomTitleEnd: "купи.",
          deactivated: false
        },
        {
          delicacyTitle: "Сказочное заморское яство",
          boxName: "Нямушка",
          boxDescr: "с курой",
          portionCount: "100 порций",
          portionGift: "5 мышей в подарок",
          boxWeight: "5",
          boxWeightUnit: "кг",
          color: "green",
          customer: "заказчик доволен",
          bottomTitle: "Печалька, с курой закончился.",
          bottomTitleEnd: "купи.",
          deactivated: true
        },
      ],
      withCustomer: true,
      withoutCustomer: true,
      disabled: true
    };
  }

  render() {
    return (
      <div className="app">
        <div className="app__wrapper">
          <div className="app__wrapper-header">
            <Title title={this.state.headerTitle} size36 />
          </div>
          <div className="app__wrapper-main">
            <ContentList 
              contents={this.state.contents} 
              withCustomer 
              withoutCustomer
              disabled
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
