import React from "react";
import Content from "./Content.jsx";
import "../assets/styles/containers/content-list.scss";
import shortid from "shortid";

const ContentList = () => ({
  render() {
    const { contents, withCustomer, withoutCustomer } = this.props;
    const singleContent = contents.map((content, i) =>
      i !== contents.length - 1 ? (
        i === 0 ? (
          <Content
            key={shortid.generate()}
            delicacyTitle={content.delicacyTitle}
            size16
            boxWeight={content.boxWeight}
            boxWeightUnit={content.boxWeightUnit}
            boxName={content.boxName}
            size48
            boxDescr={content.boxDescr}
            size24
            portionCount={content.portionCount}
            size14pc
            portionGift={content.portionGift}
            size14gift
            color={content.color}
            withCustomer={withCustomer}
            bottomTitle={content.bottomTitle}
            bottomTitleEnd={content.bottomTitleEnd}
            elementKey={i}
            deactivated={content.deactivated}
            disabled={false}
          />
        ) : (
          <Content
            key={shortid.generate()}
            delicacyTitle={content.delicacyTitle}
            size16
            boxWeight={content.boxWeight}
            boxWeightUnit={content.boxWeightUnit}
            boxName={content.boxName}
            size48
            boxDescr={content.boxDescr}
            size24
            portionCount={content.portionCount}
            size14pc
            portionGift={content.portionGift}
            size14gift
            color={content.color}
            withCustomer={withCustomer}
            bottomTitle={content.bottomTitle}
            elementKey={i}
            deactivated={content.deactivated}
            disabled={false}
          />
        )
      ) : (
        <Content
          key={shortid.generate()}
          delicacyTitle={content.delicacyTitle}
          size16
          boxWeight={content.boxWeight}
          boxWeightUnit={content.boxWeightUnit}
          boxName={content.boxName}
          size48
          boxDescr={content.boxDescr}
          size24
          portionCount={content.portionCount}
          size14pc
          portionGift={content.portionGift}
          size14gift
          customer={content.customer}
          size14customer
          color={content.color}
          withoutCustomer={withoutCustomer}
          bottomTitle={content.bottomTitle}
          elementKey={i}
          disabled
          deactivated={content.deactivated}
        />
      )
    );
    return (
      <div className="content-list">
        <div className="content-list__wrapper">
          <div className="content-list__wrapper-single">{singleContent}</div>
        </div>
      </div>
    );
  }
});

export default ContentList;
