import React, { Component } from "react";
import logo from "../assets/images/crop_cat_logo.png";
import Title from "../components/Title.jsx";
import Weight from "../components/Weight.jsx";
import "../assets/styles/containers/content.scss";
import classNames from "classnames";

class Content extends Component {
  constructor() {
    super();
    this.state = {
      scaled: false
    };
  }

  handleClick = () => {
    /*
    this.setState(prevState => ({
      scaled: !prevState.scaled
    }));
    */
    this.selectedItem();
    console.log("test");
  };

  handleBuyClick = () => {
    this.handleClick();
  };

  selectedItem() {
    this.setState(prevState => ({
      scaled: !prevState.scaled
    }));
  }

  render() {
    const {
      delicacyTitle,
      boxName,
      boxDescr,
      portionCount,
      portionGift,
      boxWeight,
      boxWeightUnit,
      customer,
      color,
      withCustomer,
      withoutCustomer,
      bottomTitle,
      bottomTitleEnd,
      disabled,
      elementKey,
      deactivated
    } = this.props;
    const contentsColor = classNames({
      content__wrapper: color === "blue",
      "content__wrapper-red": color === "red",
      "content__wrapper-green": color === "green",
      "content__wrapper-disabled": disabled
    });
    const customers = classNames({
      "content__wrapper-names": withCustomer,
      "content__wrapper-green-names-customer": withoutCustomer
    });
    const descriptions = classNames({
      description: elementKey !== 2,
      "description-yellow": elementKey === 2
    });
    const buys = classNames("buy", {
      buy__disabled: disabled
    });
    return (
      <div className="content">
        <div
          className={contentsColor}
          //onClick={this.handleClick}
          onMouseUp={this.handleClick}
          style={{ transform: this.state.scaled ? "scale(0.95)" : "scale(1)", "boxShadow": "0 0 10px rgba(150, 150, 150, 0.7)" }}
        >
          <div className={customers}>
            <div className="content__wrapper-names-delicacy">
              <Title delicacyTitle={delicacyTitle} size16 />
            </div>
            <div className="content__wrapper-names-boxname">
              <Title boxName={boxName} size48 deactivated={deactivated} />
            </div>
            <div className="content__wrapper-names-boxdescr">
              <Title boxDescr={boxDescr} size24 deactivated={deactivated} />
            </div>
            <div className="content__wrapper-names-portcount">
              <Title portionCount={portionCount} size14pc />
            </div>
            <div className="content__wrapper-names-portgift">
              <Title portionGift={portionGift} size14gift />
            </div>
            <div className="content__wrapper-names-customer">
              <Title customer={customer} size14customer />
            </div>
          </div>
          <div className="content__wrapper-details">
            <div className="content__wrapper-details-image">
              <img className="image-parameter" src={logo} alt="Cat" />
            </div>
            <div className="content__wrapper-details-weight">
              <Weight
                boxWeight={boxWeight}
                boxWeightUnit={boxWeightUnit}
                color={color}
                elementKey={elementKey}
                disabled
                deactivated={deactivated}
              />
            </div>
          </div>
        </div>
        <div className="content__wrapper-bottom">
          <span className={descriptions}>{bottomTitle}</span>
          <span className={buys} onClick={this.handleBuyClick}>
            {bottomTitleEnd}
          </span>
        </div>
      </div>
    );
  }
}

export default Content;
